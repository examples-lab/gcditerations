//
//  ViewController.swift
//  GCDIterations
//
//  Created by 金鑫 on 2020/6/9.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.concurrentPerform(iterations: 1000) { index in
            let pauseTime = arc4random() % 5
            sleep(UInt32(pauseTime))
            print(index)
        }
    }


}

